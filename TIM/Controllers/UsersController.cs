﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class UsersController : ApiController
    {
       
        public Usuario GetTodosUsuarios()
        {
            AccesoBD bd = new AccesoBD();
            string ServidorSQL = "";
            ServidorSQL = ConfigurationSettings.AppSettings["ServidorSQL"];
            string UsuarioSQL = "";
            UsuarioSQL = ConfigurationSettings.AppSettings["UsuarioSQL"];
            string PasswordSQL = "";
            PasswordSQL = ConfigurationSettings.AppSettings["PasswordSQL"];
            string database = "";
            database = ConfigurationSettings.AppSettings["database"];


            string connectionString = "";
            connectionString = "server='" + ServidorSQL + "';database='" + database + "';uid='" + UsuarioSQL + "';password='" + PasswordSQL + "'";
            string json = "";
            bd.ConnectionString = connectionString;

            List<Test> Tests = new List<Test>();
            Test TestItem = null;

            List<DbParameter> parameterList = new List<DbParameter>();

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetUsuarios", parameterList, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {

                        TestItem = new Test();
                        TestItem.SlpCode = (int)dataReader["SlpCode"];
                        TestItem.SlpName = (string)dataReader["SlpName"];
                        TestItem.Memo = (string)dataReader["Memo"];
                        TestItem.Usser = (string)dataReader["U_USSER"];
                        TestItem.Password = (string)dataReader["U_PASSWORD"];

                        Tests.Add(TestItem);
                    }
                }
            }

            Usuario usuario = new Usuario();
            usuario.usuarios = Tests;

            json = JsonConvert.SerializeObject(usuario);
            object res = json;

            return usuario;
        }
    }
    public class Test
    {
        public object SlpCode { get; internal set; }
        public object SlpName { get; internal set; }
        public object Memo { get; internal set; }
        public object Password { get; internal set; }
        public object Usser { get; internal set; }
    }
}
