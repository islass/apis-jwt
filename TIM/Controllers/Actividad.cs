﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIM.Controllers
{
    public class Actividad
    {
        public string startTime { get; set; }
        public string startdate { get; set; }
        public string endTime { get; set; }
        public string endDate { get; set; }
        public string nonBillableTime { get; set; }
    }
}