﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIM.Controllers
{
    public class Resultado
    {
        public Boolean error { get; set; }
        public string message { get; set; }
        public int EntryActivity { get; set; }
    }
}