﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TIM.Controllers
{
    public class Etapas
    {
        public List<etapa> etapas { get; set; }
    }
    public class etapa
    {
        public object StageID { get; internal set; }
        public object Name { get; internal set; }
        public object Description { get; internal set; }
    }
}