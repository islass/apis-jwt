﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TIM.Controllers
{
    [Authorize]
    public class EtapasController : ApiController
    {
        public Etapas GetEtapas()
        {
            AccesoBD bd = new AccesoBD();

            string ServidorSQL = "";
            ServidorSQL = ConfigurationSettings.AppSettings["ServidorSQL"];
            string UsuarioSQL = "";
            UsuarioSQL = ConfigurationSettings.AppSettings["UsuarioSQL"];
            string PasswordSQL = "";
            PasswordSQL = ConfigurationSettings.AppSettings["PasswordSQL"];
            string database = "";
            database = ConfigurationSettings.AppSettings["database"];


            string connectionString = "";
            connectionString = "server='" + ServidorSQL + "';database='" + database + "';uid='" + UsuarioSQL + "';password='" + PasswordSQL + "'";

            bd.ConnectionString = connectionString;
            List<etapa> Tests = new List<etapa>();
            etapa TestItem = null;

            List<DbParameter> parameterList = new List<DbParameter>();

            using (DbDataReader dataReader = bd.GetDataReader("sp_GetEtapas", parameterList, CommandType.StoredProcedure))
            {
                if (dataReader != null)
                {

                    while (dataReader.Read())
                    {

                        TestItem = new etapa();
                        TestItem.StageID = (int)dataReader["StageID"];
                        TestItem.Name = (string)dataReader["Name"];
                        TestItem.Description = (string)dataReader["Dscription"];

                        Tests.Add(TestItem);
                    }
                }
            }


            Etapas etapas = new Etapas();
            etapas.etapas = Tests;

            return etapas;
        }
    }
}
