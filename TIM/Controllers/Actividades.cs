﻿using System.Collections.Generic;

namespace TIM.Controllers
{
    public class Actividades
    {
        public List<Act> actividades { get; set; }
    }
    public class Act
    {
        public object Id { get; internal set; }
        public object CardCode { get; internal set; }
        public object CardName { get; internal set; }
        public object Activity_Name { get; internal set; }
        public object Contact { get; internal set; }
    }
}